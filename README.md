# Desafio Polvo - Vaga Dev Front

## About the Developer:

- **Nome:** Lucas Ernesto Kindinger
- **Email:** kindingerlek02@gmail.com

## Screenshots:

### Mobile:

![](./repo-assets/mobile.png)

### Tablet:

![](./repo-assets/tablet.png)

### Desktop:

![](./repo-assets/desktop-1.png)
![](./repo-assets/desktop-2.png)
![](./repo-assets/desktop-3.png)

