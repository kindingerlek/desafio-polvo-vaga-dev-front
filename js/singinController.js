
var color = ["","danger", "info", "success"];
var progress;

// Add event when password change
$( "#inputPassword" ).keyup(
    function () {
        // Making sure the elements has none style attached to it
        ResetStyle();

        var str = $("#inputPassword").val().toString();

        if(str == "")
            return;

        // Calc the progress
        progress = HasSixCharacters(str) + HasOneUpperCase(str) + HasOneNumber(str);

        //  Paint Dont
        PaintDots("#cbSixLetter", HasSixCharacters(str));
        PaintDots("#cbUpperCase", HasOneUpperCase(str));
        PaintDots("#cbLstNumber", HasOneNumber(str));

        // Paint Progress Bar
        PaintProgressBar(str);

        // Set style to this element
        $(this).addClass(color[progress]);     
    }
)

// Add event when retypePassword change
$( "#inputRetypePassword").keyup(
    function () {
        $(this).removeClass(color.join(" "));

        var password = $("#inputPassword").val();
        var retypePass = $(this).val() ;

        if (retypePass == "")
            return;

        // Set style to this element
        $(this).addClass(retypePass == password ? "success" : "danger");
    }
)

$("form").submit(function (event) {
    // Get strings
    var pass = $("#inputPassword").val().toString();
    var rePass = $("#inputRetypePassword").val().toString();

    // Calc the progress
    progress = HasSixCharacters(pass) + HasOneUpperCase(pass) + HasOneNumber(pass);
    
    // Check if any condition are not satisfied
    if(progress != 3 )
    {
        alert("Sua senha não corresponde aos requisitos. Por favor, escolha uma nova senha.");
        $("#inputRetypePassword").removeClass(color.join(' ')).val("");
        $("#inputPassword").focus();
    }
    // Check passwords does not match
    else if (pass != rePass) {
        alert("As senhas não coincidem.");
        $("#inputRetypePassword").focus();
    }
    // All right, moving on!
    else
    {
        alert("Cadastrado com sucesso!");
        return;
    }    

    // Prevent send the information to 'server'
    event.preventDefault();
})

// Clean all custom styles
function ResetStyle()
{
    progress = 0;

    $("#inputPassword").removeClass(color.join(' '));
    $('#inputRetypePassword').removeClass(color.join(' '))

    $("#cbSixLetter").removeClass(color.join(' '));
    $("#cbUpperCase").removeClass(color.join(' '));
    $("#cbLstNumber").removeClass(color.join(' '));

    PaintProgressBar();
}

function PaintProgressBar() {
    var percent = (progress / 3) * 100;
    $('#progressbar').width(percent + "%").attr('aria-valuenow', percent);
    $('#progressbar').removeClass(color.join(' ')).addClass(color[progress]);
}

function PaintDots(id, value) {
    $(id).removeClass(color.join(" ")).addClass(value ? "success" : "danger");
}

function HasSixCharacters(str) {
    return (str.length >= 6) ? 1 : 0;
}

function HasOneUpperCase(str)
{
    return (str.match(/[A-Z]/)) ? 1 : 0;
}

function HasOneNumber(str)
{
    return (str.match(/[0-9]/)) ? 1 : 0;
}